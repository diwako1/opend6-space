import {od6sroll} from "../apps/od6sroll.js";
import {od6sutilities} from "../system/utilities.js";
import OD6S from "../config/config-od6s.js";

/**
 * Extend the basic Item with some very simple modifications.
 * @extends {Item}
 */
export class OD6SItem extends Item {

    /**
     * Set the image as blank if it doesn't exist, rather than the default
     * @param data
     * @param options
     * @returns {Promise<void>}
     */
    static async create(data, options) {
        if (!data.img)
            data.img = "systems/od6s/icons/blank.png";
        super.create(data, options);
    }

    /**
     * Augment the basic Item data model with additional dynamic data.
     */
    prepareData() {
        super.prepareData();

        // Get the Item's data
        const itemData = this.data;
        const actorData = this.actor ? this.actor.data : {};
        const data = itemData.data;
        this.data.data.config = OD6S;
    }

    /**
     * Create derived data for the item
     */
    prepareDerivedData() {
        if (this.type === 'skill' || this.type === 'specialization') {
            this.data.data.score = (+this.data.data.base) + (+this.data.data.mod);
        }
    }

    /**
     * Filter the Create New Item dialog
     */
    static async createDialog(data = {}, options = {}) {
        // Collect data
        const documentName = this.metadata.name;
        let types, folders, label, title, template;
        types = game.system.documentTypes[documentName];
        folders = game.folders.filter(f => (f.data.type === documentName) && f.displayed);
        label = game.i18n.localize(this.metadata.label);
        title = game.i18n.format("OD6S.CREATE_ITEM", {entity: label});
        template = 'templates/sidebar/document-create.html';

        types = types.filter(function (value, index, arr) {
            return value !== 'action' && value !== 'vehicle';
        });

        if (game.settings.get('od6s', 'hide_advantages_disadvantages')) {
            types = types.filter(function (value, index, arr) {
                return value !== 'advantage';
            })
            types = types.filter(function (value, index, arr) {
                return value !== 'disadvantage';
            })
        }

        types = types.sort(function (a, b) {
            return a.localeCompare(b);
        })

        // Render the entity creation form
        const html = await renderTemplate(template, {
            name: data.name || game.i18n.format("OD6S.NEW_ITEM", {entity: label}),
            folder: data.folder,
            folders: folders,
            hasFolders: folders.length > 1,
            type: data.type || types[0],
            types: types.reduce((obj, t) => {
                const label = CONFIG[documentName]?.typeLabels?.[t] ?? t;
                obj[t] = game.i18n.has(label) ? game.i18n.localize(label) : t;
                return obj;
            }, {}),
            hasTypes: types.length > 1
        });

        // Render the confirmation dialog window
        return Dialog.prompt({
            title: title,
            content: html,
            label: title,
            callback: html => {
                const form = html[0].querySelector("form");
                const fd = new FormDataExtended(form);
                data = foundry.utils.mergeObject(data, fd.toObject());
                if (!data.folder) delete data["folder"];
                if (types.length === 1) data.type = types[0];
                return this.create(data, {renderSheet: true});
            },
            rejectClose: false,
            options: options
        });
    }

    /**
     * Handle clickable item rolls.
     * @private
     */
    async roll(parry = false) {
        // Basic template rendering data
        const token = this.actor.token;
        const item = this;
        const actor = this.actor ? this.actor : {};
        const actorData = this.actor ? this.actor.data.data : {};
        const itemData = item.data.data;

        const rollData = {};
        switch (item.type) {
            case 'attribute': {
                return;
            }
            case 'skill': {
                rollData.score = (+itemData.score) + actorData.attributes[itemData.attribute.toLowerCase()].score;
                break;
            }
            case 'specialization': {
                rollData.score = (+itemData.score) + actorData.attributes[itemData.attribute.toLowerCase()].score;
                break;
            }
            case 'weapon': {
                // Try a specialization first, then a skill, then an attribute
                let found = false;

                if (parry) {
                    const skill = actor.items.find(skill => skill.name === game.i18n.localize(OD6S.actions.parry.skill) && skill.type === 'skill');
                    if (skill) {
                        rollData.score = (+skill.data.data.score) + (+actorData.attributes[skill.data.data.attribute.toLowerCase()].score);
                    } else {
                        rollData.score = actorData.attributes[OD6S.actions.parry.base.toLowerCase()].score;
                    }
                    found = true;
                }

                if (!found && itemData.stats.specialization !== null) {
                    const spec = actor.items.find(spec => spec.name === itemData.stats.specialization && spec.type === 'specialization');
                    if (spec) {
                        rollData.score = (+spec.data.data.score) + (+actorData.attributes[spec.data.data.attribute.toLowerCase()].score);
                        found = true;
                    }
                }
                if (!found) {
                    // See if the actor has the associated skill
                    const skill = actor.items.find(skill => skill.name === itemData.stats.skill && skill.type === 'skill');
                    if (typeof (skill) !== 'undefined' && skill !== null) {
                        rollData.score = (+skill.data.data.score) + (+actorData.attributes[skill.data.data.attribute.toLowerCase()].score);
                    } else {
                        // Finally, use base attribute
                        rollData.score = actorData.attributes[itemData.stats.attribute.toLowerCase()].score;
                    }
                }
                break;
            }
            case 'action': {
                let name = '';
                if ((itemData.subtype === 'rangedattack' || itemData.subtype === 'meleeattack') && itemData.itemId !== '') {
                    // Roll is linked to an inventory item, roll that instead
                    const targetItem = actor.items.find(i => i.id === itemData.itemId);
                    return targetItem.roll(parry);
                }

                if (itemData.subtype === 'dodge' || itemData.subtype === 'parry' || itemData.subtype === 'block') {
                    // Get the appropriate skill or attribute
                    switch (itemData.subtype) {
                        case 'dodge':
                            name = 'OD6S.DODGE';
                            break;
                        case 'parry':
                            if (actor.items.find(i => i.id === itemData.itemId)) {
                                const targetItem = actor.items.find(i => i.id === itemData.itemId);
                                return targetItem.roll(true);
                            } else {
                                name = OD6S.actions.parry.skill;
                            }
                            break;
                        case 'block':
                            name = OD6S.actions.parry.skill;
                            break;
                    }
                }

                if (itemData.subtype === 'attribute') {
                    rollData.attribute = itemData.itemId;
                } else {
                    let skill = '';
                    //let name = item.name;
                    name = game.i18n.localize(name);
                    if (typeof (itemData.itemId) !== 'undefined' && itemData.itemId !== '') {
                        skill = actor.items.find(i => i.type === itemData.subtype && i.id === itemData.itemId);
                    } else {
                        skill = actor.items.find(i => i.name === name);
                    }
                    if (skill !== null && typeof (skill) !== 'undefined' && typeof (skill.data.data.score) !== 'undefined') {
                        rollData.score = (+skill.data.data.score) + (+actorData.attributes[skill.data.data.attribute.toLowerCase()].score);
                    } else {
                        // Search compendia for the skill and use the attribute
                        // rollData.score = (+actorData.attributes['agi'].score);
                        skill = await od6sutilities._getItemFromCompendium(name);
                        if (skill !== null && typeof (skill) !== 'undefined') {
                            rollData.score = (+skill.data.data.score) + (+actorData.attributes[skill.data.data.attribute.toLowerCase()].score);
                        } else {
                            skill = await od6sutilities._getItemFromWorld(name);
                            if (skill !== null && typeof (skill) !== 'undefined') {
                                rollData.score = (+actorData.attributes[skill.data.data.attribute.toLowerCase()].score);
                            }
                        }
                    }
                }

                break;
            }
        }

        let subtype = itemData.subtype;
        if (parry) {
            subtype = "parry";
        }

        rollData.name = item.name;
        rollData.type = item.type;
        rollData.actor = this.actor;
        rollData.itemId = item.id;
        rollData.subtype = subtype;

        await od6sroll._onRollDialog(rollData);
    }
}
